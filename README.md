# urw-base35-fonts

The Level 2 Core Font Set is a PostScript specification of 35 base fonts that can be used with any PostScript file. In Fedora, these fonts are provided freely by (URW)++ company, and are mainly utilized by applications using Ghostscript.